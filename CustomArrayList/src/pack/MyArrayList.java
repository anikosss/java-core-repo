package pack;

public class MyArrayList<T> {
    private static final int DEFAULT_SIZE = 10;
    private static final double DEFAULT_OVERFLOW_PARAM = 0.8;
    private static final int MAX_SIZE = Integer.MAX_VALUE - 8;
    private Object[] values;
    private int size;
    private double overflowParam;
    private int count = 0;

    public MyArrayList() {
        size = DEFAULT_SIZE;
        overflowParam = DEFAULT_OVERFLOW_PARAM;
        values = new Object[size];
    }

    public MyArrayList(int size) {
        if (size <= 0 || size > MAX_SIZE) {
            throw new IllegalArgumentException("Wrong initial size:" + size);
        }
        this.size = size;
        overflowParam = DEFAULT_OVERFLOW_PARAM;
        values = new Object[size];
    }

    public MyArrayList(double overflowParam) {
        if (overflowParam <= 0 || overflowParam > 1) {
            throw new IllegalArgumentException("Wrong overflow param:" + overflowParam);
        }
        this.overflowParam = overflowParam;
        size = DEFAULT_SIZE;
        values = new Object[size];
    }

    public MyArrayList(int size, double overflowParam) {
        if (size <= 0 || size > MAX_SIZE || overflowParam <= 0 || overflowParam > 1) {
            throw new IllegalArgumentException("Wrong initial params: size = " + size + ", overFlowParam = " + overflowParam);
        }
        this.size = size;
        this.overflowParam = overflowParam;
        values = new Object[size];
    }

    public void add(T element) {
        checkSize();
        values[count] = element;
        count++;
    }

    public void set(int index, T element) {
        if (index < 0 || index > count -1) {
            throw new IllegalArgumentException("Wrong index: " + index);
        }
        values[index] = element;
        count++;
    }

    public T get(int index) {
        if (index < 0 || index > count - 1) {
            throw new IllegalArgumentException("Wrong index:" + index);
        }
        T result = (T) values[index];
        return result;
    }

    public int size() {
        return count;
    }

    public boolean isEmpty() {
        return (count == 0);
    }

    public void remove(int index) {
        if (index < 0 || index > count -1) {
            throw new IllegalArgumentException("Wrong index: " + index);
        }
        values[index] = null;
        rearrange(index);
        count--;
    }

    public void removeAll() {
        for (int i = 0; i < count; i++) {
            values[i] = null;
        }
        count = 0;
    }

    private void checkSize() {
        double a = (double) count / size;
        if (a >= overflowParam) {
            copy();
        }
    }

    private void copy() {
        int newSize = (int) (size * 3)/2 + 1;
        Object[] newValues = new Object[newSize];
        System.arraycopy(values, 0, newValues, 0, count);
        size = newSize;
        values = newValues;
    }

    private void rearrange(int index) {
        for (int i = index; i < count - 1; i++) {
            values[i] = values[i + 1];
        }
        values[count - 1] = null;
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder("[");
        for (int i = 0; i < count; i++) {
            result.append("{").append(values[i].toString()).append("}");
        }
        return result.append("]").toString();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj.getClass() != getClass()) {
            return false;
        }
        MyArrayList list = (MyArrayList) obj;
        if (list.size != size) {
            return false;
        }
        if (list.overflowParam != overflowParam) {
            return false;
        }
        if (list.count != count) {
            return false;
        }
        for (int i = 0; i < list.size; i++) {
            if (!list.get(i).equals(get(i))) {
                return false;
            }
        }
        return true;
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = Arrays.hashCode(values);
        result = 31 * result + size;
        temp = Double.doubleToLongBits(overflowParam);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + count;
        return result;
    }

}